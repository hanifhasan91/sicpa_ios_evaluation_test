//
//  ApiWebservice.swift
//  sicpa evaluation test
//
//  Created by Muhammad Hanif Bin Hasan on 22/02/2022.
//

import Foundation

final class ApiWebservice {
    
    func getArticlesPopular(url: String , completionHandler: @escaping (ArticlePopular) -> Void) {

        let urlString = "\(url)api-key=\(Constant().nytKey)"
        print("apiUrl getArticlesPopular -> \(urlString)")
        //remove spacing
        let urlNoPercent = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        guard let url = URL(string: urlNoPercent) else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("Error with getArticles: \(error)")
                return
            }

            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                    print("Error with the response, unexpected status code: \(String(describing: response))")
                    return
            }

            guard let data = data else { return }
            //Implement JSON decoding and parsing
            do {
                //Decode retrived data with JSONDecoder and assing type of Article object
                let utf8Data = String(decoding: data, as: UTF8.self).data(using: .utf8)

                let jSONData = try JSONDecoder().decode(ArticlePopular.self, from: utf8Data!)
                completionHandler(jSONData)


            } catch let jsonError {
                print(jsonError)
            }
        })
        task.resume()
    }
    
    func getArticlesSearch(url: String , completionHandler: @escaping (ArticleSearch) -> Void) {

        let urlString = "\(url)api-key=\(Constant().nytKey)"
        print("apiUrl getArticlesSearch -> \(urlString)")
        //remove spacing
        let urlNoPercent = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        guard let url = URL(string: urlNoPercent) else { return }
        
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            if let error = error {
                print("Error with getArticles: \(error)")
                return
            }

            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                    print("Error with the response, unexpected status code: \(String(describing: response))")
                    return
            }

            guard let data = data else { return }
            //Implement JSON decoding and parsing
            do {
                //Decode retrived data with JSONDecoder and assing type of Article object
                let utf8Data = String(decoding: data, as: UTF8.self).data(using: .utf8)

                let jSONData = try JSONDecoder().decode(ArticleSearch.self, from: utf8Data!)
                completionHandler(jSONData)


            } catch let jsonError {
                print(jsonError)
            }
        })
        task.resume()
    }
    
}
