//
//  ArticleSearchModel.swift
//  sicpa evaluation test
//
//  Created by Muhammad Hanif Bin Hasan on 23/02/2022.
//

import Foundation

struct ArticleSearch: Codable {
    let status, copyright: String
    let response: SearchResponse
}

// MARK: - SearchResponse
struct SearchResponse: Codable {
    let docs: [Documents]

}

// MARK: - Documents
struct Documents: Codable {
    let headline: Headline
    let pubDate: String

    enum CodingKeys: String, CodingKey {
        case headline
        case pubDate = "pub_date"
    }
}

// MARK: - Headline
struct Headline: Codable {
    let main: String
    let printHeadline: String?

    enum CodingKeys: String, CodingKey {
        case main
        case printHeadline = "print_headline"
    }
}
