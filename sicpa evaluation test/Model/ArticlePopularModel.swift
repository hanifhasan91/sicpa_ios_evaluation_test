//
//  ArticleModel.swift
//  sicpa evaluation test
//
//  Created by Muhammad Hanif Bin Hasan on 22/02/2022.
//

import Foundation

// MARK: - Articles
struct ArticlePopular : Codable {
    let status, copyright: String
    let results: [Result]
    
    enum CodingKeys: String, CodingKey {
        case status, copyright
        case results
    }
}

// MARK: - Result
struct Result: Codable {
    let title : String
    let publishedDt : String

    enum CodingKeys: String, CodingKey {
        case publishedDt = "published_date"
        case title
    }
}

