//
//  ListController.swift
//  sicpa evaluation test
//
//  Created by Muhammad Hanif Bin Hasan on 22/02/2022.
//

import UIKit
import NVActivityIndicatorView

class ListController: UIViewController, NVActivityIndicatorViewable {
    
    //Mark- Properties
    @IBOutlet weak var tblArticle: UITableView!
    
    //Mark- varialble
    var objArticle : ArticlePopular?
    var ArticleType : String = ""
    var url : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
        apiGet()
    }
    
    func configureView() {
        tblArticle.dataSource = self
        tblArticle.delegate = self
    }
    
    @IBAction func doBtnBack(_ sender: Any) {
        backNavigationController()
    }
    
    func apiGet() {
        
        switch ArticleType {
        case "viewed": url =  Constant().urlMostViewed
        case "shared":  url =  Constant().urlMostShared
        case "email": url =  Constant().urlMostEmail
        case "search": url =  Constant().urlSearch
        default: break
            
        }
        
        //Start loader
        startAnimating()
        
        ApiWebservice().getArticlesPopular(url: url) { (Result) in
            DispatchQueue.main.async{

                self.objArticle = Result
                self.tblArticle.reloadData()
                
                //Stop loader
                self.stopAnimating()
            }
        }
    }

}

extension ListController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if objArticle == nil {
            return 0
        }else {
            return (objArticle?.results.count)!
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblArticle.dequeueReusableCell(withIdentifier: "idarticletblcell", for: indexPath) as! ArticleTblCell
        
        let item : Result = (objArticle?.results[indexPath.row])!
        cell.lblTitle.text = item.title
        cell.lblPublishDt.text = item.publishedDt
        
        return cell
    }
}
