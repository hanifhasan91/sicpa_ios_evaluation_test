//
//  SearchController.swift
//  sicpa evaluation test
//
//  Created by Muhammad Hanif Bin Hasan on 22/02/2022.
//

import UIKit
import NVActivityIndicatorView

class SearchController: UIViewController, NVActivityIndicatorViewable {

    //Mark- Properties
    @IBOutlet weak var tblArticle: UITableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var viewNoArticle: UIView!
  
    var objArticle : ArticleSearch?
    var url : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configureView()
    }
    
    
    func configureView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        txtSearch.borderRadiusView(border: 1, radius: 10, color: .darkGray)
        txtSearch.delegate = self
        
        tblArticle.dataSource = self
        tblArticle.delegate = self
    }
    
    @IBAction func doBtnBack(_ sender: Any) {
        backNavigationController()
    }
    
    @IBAction func doBtnSearch(_ sender: Any) {
        
        dismissKeyboard()
        apiGet()
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    func apiGet() {
        url = "\(Constant().urlSearch)&fq=\(txtSearch.text!)&q=\(txtSearch.text!)&"
        
        //Start loader
        startAnimating()
        
        ApiWebservice().getArticlesSearch(url: url) { (Result) in
            DispatchQueue.main.async{
             
                self.objArticle = Result
                if self.objArticle != nil {
                    if self.objArticle?.response.docs.count != 0 {
                        self.tblArticle.isHidden = false
                        self.tblArticle.reloadData()
                    }else {
                        self.tblArticle.isHidden = true
                    }
                }
                //Stop loader
                self.stopAnimating()
            }
        }
    }
    
}

extension SearchController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if objArticle == nil {
            return 0
        }else {
            return (objArticle?.response.docs.count)!
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblArticle.dequeueReusableCell(withIdentifier: "idarticletblcell", for: indexPath) as! ArticleTblCell
        
        let item : Documents = (objArticle?.response.docs[indexPath.row])!
        cell.lblTitle.text = item.headline.main
        cell.lblPublishDt.text = item.pubDate
        
        return cell
    }
}

extension SearchController: UITextFieldDelegate{
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        apiGet()
        return true;
    }
}
