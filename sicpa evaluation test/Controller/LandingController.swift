//
//  LandingController.swift
//  sicpa evaluation test
//
//  Created by Muhammad Hanif Bin Hasan on 22/02/2022.
//

import UIKit

class LandingController: UIViewController {
    
    var ArticleType : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func doBtnSearch(_ sender: UIButton) {
        performSegue(withIdentifier: "seguesearch", sender: self)
    }
    
    @IBAction func doBtnPopular(_ sender: UIButton) {
        //get from tag button in storyboard
        switch sender.tag {
        case 1: ArticleType = "viewed"
        case 2: ArticleType = "shared"
        case 3: ArticleType = "email"
        default:
            break
        }
        vcList(vc: self, type: ArticleType)
    }
    
    @IBAction func doBtnBack(_ sender: Any) {
        backNavigationController()
    }
    
}
