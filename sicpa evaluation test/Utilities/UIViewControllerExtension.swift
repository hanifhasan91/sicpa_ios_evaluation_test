//
//  UIViewControllerExtension.swift
//  sicpa evaluation test
//
//  Created by Muhammad Hanif Bin Hasan on 23/02/2022.
//

import Foundation
import UIKit

extension UIViewController {
    
    @objc func backNavigationController() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func vcList(vc: UIViewController, type: String) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "listsbid") as! ListController
        vc.ArticleType = type
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
