//
//  Constant.swift
//  sicpa evaluation test
//
//  Created by Muhammad Hanif Bin Hasan on 22/02/2022.
//

import Foundation

class Constant {
    var nytKey = "fwZGPqJNn0b36Q6e6Bvja7kmjm2R1QO1"
    var urlMostViewed = "https://api.nytimes.com/svc/mostpopular/v2/viewed/7.json?"
    var urlMostShared = "https://api.nytimes.com/svc/mostpopular/v2/shared/7/facebook.json?"
    var urlMostEmail = "https://api.nytimes.com/svc/mostpopular/v2/emailed/7.json?"
    var urlSearch = "https://api.nytimes.com/svc/search/v2/articlesearch.json?"
}
