//
//  UIViewExtension.swift
//  sicpa evaluation test
//
//  Created by Muhammad Hanif Bin Hasan on 22/02/2022.
//

import Foundation
import UIKit

extension UIView {

    func borderRadiusView(border: CGFloat, radius : CGFloat, color: UIColor) {
        self.layer.borderWidth = border
        self.layer.cornerRadius = radius
        self.layer.borderColor = color.cgColor
        self.layer.masksToBounds = true
    }
    
    func addShadow(conerRadius : CGFloat,shadowRadius : CGFloat, color: UIColor) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowRadius = shadowRadius
        layer.cornerRadius = conerRadius
        layer.shadowOffset = CGSize(width: 0, height: 2)
     }
    
}
